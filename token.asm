            ; Write record 10000 to SUPPLY
            ; write record ^1 to ^2
PUSH 0x2710
PUSH 0x20
SSTORE
            ; Write 10000 to CONST_ORIGIN related record 32
            ; write ^1 to ^2 related record ^3
PUSH 0x2710
ORIGIN
PUSH 0x0100
MUL
PUSH 0x20
ADD
SSTORE
            ; Write record CONST_ORIGIN to OWNER
            ; write record ^1 to ^2
ORIGIN
PUSH 0x40
SSTORE
            ; Sections
            ; sections
INIT
            ; Revenue section
            ; revenue section
FALLBACK ()
            ; Stop
            ; stop
STOP
            ; constant balanceOf(address)
            ; constant ^1
JUMPDEST balanceOf(address)
            ; Let ADDRESS get argument
            ; let ^1 get argument
PUSH 0x04
CALLDATALOAD
PUSH 0x60
MSTORE
            ; Let BALANCE read ADDRESS related record 32
            ; let ^1 read ^2 related record ^3
PUSH 0x60
MLOAD
PUSH 0x0100
MUL
PUSH 0x20
ADD
SLOAD
PUSH 0x80
MSTORE
            ; return uint256 at BALANCE
            ; return uint256 at ^1
PUSH 0x20
PUSH 0x80
RETURN
            ; constant totalSupply()
            ; constant ^1
JUMPDEST totalSupply()
            ; Grab record SUPPLY
            ; grab record ^1
PUSH 0x20
DUP1
SLOAD
SWAP1
MSTORE
            ; return uint256 at SUPPLY
            ; return uint256 at ^1
PUSH 0x20
PUSH 0x20
RETURN
            ; jumpdest transfer(address,uint256)
            ; jumpdest ^1
JUMPDEST transfer(address,uint256)
            ; Let VALUE get argument 2
            ; let ^1 get argument ^2
PUSH 0x20
PUSH 0x02
MUL
PUSH 0x1C
SUB
CALLDATALOAD
PUSH 0xa0
MSTORE
            ; Let MY_BALANCE read CALLER related record 32
            ; let ^1 read ^2 related record ^3
CALLER
PUSH 0x0100
MUL
PUSH 0x20
ADD
SLOAD
PUSH 0xc0
MSTORE
            ; if VALUE > MY_BALANCE invoke 100
            ; if ^1 > ^2 invoke ^3
PUSH 0xc0
MLOAD
PUSH 0xa0
MLOAD
GT
JUMPI tag100:
            ; Let RECEIVER get argument
            ; let ^1 get argument
PUSH 0x04
CALLDATALOAD
PUSH 0xe0
MSTORE
            ; Let RECEIVER_BALANCE read RECEIVER related record 32
            ; let ^1 read ^2 related record ^3
PUSH 0xe0
MLOAD
PUSH 0x0100
MUL
PUSH 0x20
ADD
SLOAD
PUSH 0x0100
MSTORE
            ; Decrement MY_BALANCE by VALUE
            ; decrement ^1 by ^2
PUSH 0xa0
MLOAD
PUSH 0xc0
MLOAD
SUB
PUSH 0xc0
MSTORE
            ; Increment RECEIVER_BALANCE by VALUE
            ; increment ^1 by ^2
PUSH 0xa0
MLOAD
PUSH 0x0100
MLOAD
ADD
PUSH 0x0100
MSTORE
            ; Write MY_BALANCE to CALLER related record 32
            ; write ^1 to ^2 related record ^3
PUSH 0xc0
MLOAD
CALLER
PUSH 0x0100
MUL
PUSH 0x20
ADD
SSTORE
            ; Write RECEIVER_BALANCE to RECEIVER related record 32
            ; write ^1 to ^2 related record ^3
PUSH 0x0100
MLOAD
PUSH 0xe0
MLOAD
PUSH 0x0100
MUL
PUSH 0x20
ADD
SSTORE
            ; Let SUCCESS = 1
            ; let ^1 = ^2
PUSH 0x01
PUSH 0x0120
MSTORE
            ; return bool at SUCCESS
            ; return bool at ^1
PUSH 0x01
PUSH 0x0120
RETURN
            ; constant owner()
            ; constant ^1
JUMPDEST owner()
            ; Grab record OWNER
            ; grab record ^1
PUSH 0x40
DUP1
SLOAD
SWAP1
MSTORE
            ; return address at OWNER
            ; return address at ^1
PUSH 0x14
PUSH 0x40
PUSH 0x0C
ADD
RETURN
            ; jumpdest suicide()
            ; jumpdest ^1
JUMPDEST suicide()
            ; suicide
            ; suicide
SUICIDE
            ; return
            ; return
STOP
            ; jumpdest 100
            ; jumpdest ^1
JUMPDEST tag100:
            ; Stop
            ; stop
STOP
